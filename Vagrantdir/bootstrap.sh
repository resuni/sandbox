#!/bin/sh

# Replace default Nginx config with our own & restart
rm -f /etc/nginx/conf.d/default.conf
cp /vagrant/Vagrantdir/nginx.conf /etc/nginx/conf.d/default.conf
sed -i 's/    sendfile        on;/    sendfile        off;/' /etc/nginx/nginx.conf
systemctl restart nginx
